# CUDA noteshrink

Allows to shrink and clear scanned, handwritten notes.
CUDA implementation of algorithm presented [here](https://mzucker.github.io/2016/09/20/noteshrink.html).

## Installation & Running
```
cmake .
make
./bin/cuda_noteshrink
```
## Authors
Angelika Serwa

Weronika Grzybowska

## CUDA wrapper usage
CUDA wrapper should automatically check for CUDA errors,
print neat error messages, release memory and synchronize 
the context. This code looks better for me than the standard one.
Be warned though that it wasn't tested properly ;)

``` c++
CUDADriver cudaDriver = CUDADriver();
CUDAModule doubleModule = cudaDriver.loadModule("double.ptx");
CUDAFunction doubleFunction = doubleModule.getFunction("doubleFunction");
int n = 16;
CUDAHostMemory<int> hostMemory = cudaDriver.allocHostMemory<int>(n);
CUDADeviceMemory deviceMemory = cudaDriver.allocDeviceMemory(n*sizeof(int));
for(int i = 0; i < n; ++i)
    hostMemory[i] = i;
cudaDriver.copyHostToDevice(deviceMemory, hostMemory);
doubleFunction.launchKernel(1, 1, &n, deviceMemory);
cudaDriver.copyDeviceToHost(hostMemory, deviceMemory);
for(int i = 0; i < n; ++i)
    std::cout << hostMemory[i] << std::endl;
```

## cmake usage
We can just add the new *.cpp, *.h and *cu files, run `cmake .` 
command and the new files should be added to the makefile.
*.cu files are compiled to *.ptx file in bin directory.

To generate debug binary run `cmake -DCMAKE_BUILD_TYPE=Debug .` and then `make`.