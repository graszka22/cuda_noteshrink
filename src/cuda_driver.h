#ifndef CUDA_DRIVER_H
#define CUDA_DRIVER_H
#include <string>
#include <cuda.h>
#include <exception>
#include <cstring>
#include <memory>
#include <functional>
class CUDAException : public std::exception {
    public:
        CUDAException(CUresult cudaRes) : errCode(cudaRes) { }
        const char* what() const noexcept override {
            const char* errorName;
            const char* errorDescription;
            cuGetErrorName(errCode, &errorName);
            cuGetErrorString(errCode, &errorDescription);
            char* cstr = new char[strlen(errorName)+strlen(errorDescription)+5];
            cstr[0] = 0;
            strcat(cstr, errorName);
            strcat(cstr, ": ");
            strcat(cstr, errorDescription);
            return cstr;
        }
        static void tryCuda(CUresult result) {
            if(result != CUDA_SUCCESS)
                throw CUDAException(result);
        }
    private:
        CUresult errCode;
};
class CUDADriver;
class CUDAFunction;
class CUDAModule;
class CUDADeviceMemory;

class CUDAFunction {
    public:
        template<class... T>
        void launchKernel(int blocksX, int threadsX, T... t) {
            launchKernelMultiDim(blocksX, 1, 1, threadsX, 1, 1, t...);
        }

        template<class... T>
        void launchKernelMultiDim(int blocksX, int blocksY, int blocksZ,
            int threadsX, int threadsY, int threadsZ, T... t) {
            void* args[] = {t...};
            CUDAException::tryCuda(cuLaunchKernel(function, blocksX, blocksY, blocksZ,
                threadsX, threadsY, threadsZ, 0, 0, args, 0));
            CUDAException::tryCuda(cuCtxSynchronize());
        }

    private:
         CUDAFunction(CUfunction function) : function(function) {}
         CUfunction function;
    friend class CUDAModule;
};

class CUDAModule {
    public:
        CUDAFunction getFunction(std::string functionName) {
            CUfunction cuFunction;
            CUDAException::tryCuda(cuModuleGetFunction(&cuFunction, module, functionName.c_str()));
            return CUDAFunction(cuFunction);
        }
    private:
        CUDAModule(CUmodule module) : module(module) {}
        CUmodule module;
    friend class CUDADriver;
};

template<class T>
class CUDAHostMemory {
    public:
        CUDAHostMemory() {}
        CUDAHostMemory(CUDAHostMemory<T>& other) {
            elementsCount = other.elementsCount;
            pointer = other.pointer;
        }
        CUDAHostMemory(CUDAHostMemory<T>&& other) {
            elementsCount = other.elementsCount;
            pointer = other.pointer;
            other.pointer = std::shared_ptr<T>();
        }
        CUDAHostMemory<T>& operator=(const CUDAHostMemory<T>& other) {
            elementsCount = other.elementsCount;
            pointer = other.pointer;
            return *this;
        }
        CUDAHostMemory<T>& operator=(CUDAHostMemory<T>&& other) {
            if(this != &other) {
                elementsCount = other.elementsCount;
                pointer = other.pointer;
                other.pointer = std::shared_ptr<T>();
            }
            return *this;
        }
        size_t getElementsCount() {
            return elementsCount;
        }
        size_t getSizeInBytes() {
            return elementsCount*sizeof(T);
        }
        template<class S>
        void copyData(S* data, size_t bytes) {
            memcpy(pointer.get(), data, bytes);
        }
        template<class S>
        void copyDataTo(S* data, size_t bytes) {
            memcpy(data, pointer.get(), bytes);
        }
        T& operator[](std::size_t idx) {
            return pointer.get()[idx];
        }
        const T& operator[](std::size_t idx) const {
            return pointer.get()[idx];
        }
    private:
        CUDAHostMemory(T* hostPointer, size_t elementsCount) : elementsCount(elementsCount) {
            pointer = std::shared_ptr<T>(hostPointer, [](T* ptr) {
                cuMemFreeHost(ptr);
            });
        }
        std::shared_ptr<T> pointer;
        size_t elementsCount;
    friend class CUDADriver;
};

class CUDADeviceMemory {
    public:
        CUDADeviceMemory() {}
        CUDADeviceMemory(CUDADeviceMemory& other) {
            size = other.size;
            pointer = other.pointer;
            ptr = other.ptr;
        }
        CUDADeviceMemory(CUDADeviceMemory&& other) {
            size = other.size;
            ptr = other.ptr;
            pointer = other.pointer;
            other.pointer = std::shared_ptr<CUdeviceptr>();
        }
        CUDADeviceMemory& operator=(const CUDADeviceMemory& other) {
            size = other.size;
            ptr = other.ptr;
            pointer = other.pointer;
            return *this;
        }
        CUDADeviceMemory& operator=(CUDADeviceMemory&& other) {
            if(this != &other) {
                size = other.size;
                ptr = other.ptr;
                pointer = other.pointer;
                other.pointer = std::shared_ptr<CUdeviceptr>();
            }
            return *this;
        }
        operator void*() const { 
            return (void*)(&(*pointer));
        }
        size_t getSizeInBytes() {
            return size;
        }
    private:
        CUDADeviceMemory(CUdeviceptr devicePointer, size_t size) : size(size) {
            ptr = devicePointer;
            pointer = std::shared_ptr<CUdeviceptr> (&ptr, [](CUdeviceptr* ptr) {
                cuMemFree(*ptr);
            });
        }
        std::shared_ptr<CUdeviceptr> pointer;
        CUdeviceptr ptr;
        size_t size;
    friend class CUDADriver;
};

class CUDADriver {
    public:
        CUDADriver() {
            CUdevice cuDevice;
            CUDAException::tryCuda(cuInit(0));
            CUDAException::tryCuda(cuDeviceGet(&cuDevice, 0));
            CUDAException::tryCuda(cuCtxCreate(&(this->context), 0, cuDevice));
        }
        ~CUDADriver() {
            CUDAException::tryCuda(cuCtxDestroy(context));
        }
        CUDAModule loadModule(std::string module) {
            CUmodule cuModule;
            CUDAException::tryCuda(cuModuleLoad(&cuModule, module.c_str()));
            return CUDAModule(cuModule);
        }

        template<class T>
        CUDAHostMemory<T> allocHostMemory(size_t count) {
            T* pointer;
            CUDAException::tryCuda(cuMemAllocHost((void**)(&pointer), count*sizeof(T)));
            return CUDAHostMemory<T>(pointer, count);
        }
        CUDADeviceMemory allocDeviceMemory(size_t bytes) {
            CUdeviceptr pointer;
            CUDAException::tryCuda(cuMemAlloc(&pointer, bytes));
            return CUDADeviceMemory(pointer, bytes);
        }
        void copyDeviceToDevice(CUDADeviceMemory dst, CUDADeviceMemory src) {
            copyDeviceToDevice(dst, src, std::min(dst.getSizeInBytes(), src.getSizeInBytes()));
        }
        template<class T>
        void copyHostToDevice(CUDADeviceMemory dst, CUDAHostMemory<T> src) {
            copyHostToDevice(dst, src, std::min(dst.getSizeInBytes(), src.getSizeInBytes()));
        }
        template<class T>
        void copyDeviceToHost(CUDAHostMemory<T> dst, CUDADeviceMemory src) {
            copyDeviceToHost(dst, src, std::min(dst.getSizeInBytes(), src.getSizeInBytes()));
        }
        template<class T, class S>
        void copyHostToHost(CUDAHostMemory<T> dst, CUDAHostMemory<S> src) {
            copyHostToHost(dst, src, std::min(dst.getSizeInBytes(), src.getSizeInBytes()));
        }
        void copyDeviceToDevice(CUDADeviceMemory dst, CUDADeviceMemory src, size_t bytes) {
            CUDAException::tryCuda(cuMemcpyDtoD(*(dst.pointer), *(src.pointer), bytes));
        }
        template<class T>
        void copyHostToDevice(CUDADeviceMemory dst, CUDAHostMemory<T> src, size_t bytes) {
            CUDAException::tryCuda(cuMemcpyHtoD(*dst.pointer, src.pointer.get(), bytes));
        }
        template<class T>
        void copyDeviceToHost(CUDAHostMemory<T> dst, CUDADeviceMemory src, size_t bytes) {
            CUDAException::tryCuda(cuMemcpyDtoH(dst.pointer.get(), *src.pointer, bytes));
        }
        template<class T, class S>
        void copyHostToHost(CUDAHostMemory<T> dst, CUDAHostMemory<S> src, size_t bytes) {
            memcpy(dst.pointer.get(), src.pointer.get(), bytes);
        }
    private:
        CUcontext context;
};

#endif
