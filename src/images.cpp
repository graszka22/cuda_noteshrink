#include <stdio.h>
#include <jpeglib.h>
#include "lodepng.h"

void readJpegImage(char * filename, unsigned * out_width, unsigned * out_height, uint32_t ** out_pixels) {
    jpeg_decompress_struct cinfo;
    jpeg_error_mgr jerr;

    FILE * infile;
    if ((infile = fopen(filename, "rb")) == NULL) {
        fprintf(stderr, "can't open %s\n", filename);
        exit(1);
    }

    cinfo.err = jpeg_std_error(&jerr);
    jpeg_create_decompress(&cinfo);
    jpeg_stdio_src(&cinfo, infile);

    jpeg_read_header(&cinfo, TRUE);
    jpeg_start_decompress(&cinfo);

    unsigned width = *out_width = cinfo.output_width;
    unsigned height = *out_height = cinfo.output_height;
    *out_pixels = new uint32_t [width * height];
    uint8_t * pixels_it = (uint8_t*)(*out_pixels);

    int row_stride = width * cinfo.output_components;
    JSAMPARRAY jpeg_buffer = (*cinfo.mem->alloc_sarray) ((j_common_ptr) &cinfo, JPOOL_IMAGE, row_stride, 1);

    uint8_t r, g, b;
    while (cinfo.output_scanline < height) {
        jpeg_read_scanlines(&cinfo, jpeg_buffer, 1);

        for (unsigned x = 0; x < width; x++) {
            r = jpeg_buffer[0][cinfo.output_components * x];
            if (cinfo.output_components > 2) {
                g = jpeg_buffer[0][cinfo.output_components * x + 1];
                b = jpeg_buffer[0][cinfo.output_components * x + 2];
            } else {
                g = r;
                b = r;
            }

            *(pixels_it++) = r;
            *(pixels_it++) = g;
            *(pixels_it++) = b;
            *(pixels_it++) = 0xff;
        }
    }

    fclose(infile);
    jpeg_finish_decompress(&cinfo);
    jpeg_destroy_decompress(&cinfo);
}

void writePngImage(char * filename, unsigned width, unsigned height, uint32_t * pixels) {
    lodepng::encode(std::string(filename), (unsigned char*)pixels, width, height);
}
