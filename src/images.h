#ifndef NOTESHRINK_IMAGES_H
#define NOTESHRINK_IMAGES_H

void readJpegImage(char * filename, unsigned * out_width, unsigned * out_height, uint32_t ** out_pixels);
void writePngImage(char * filename, unsigned width, unsigned height, uint32_t * pixels);

#endif
