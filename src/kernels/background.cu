#include <cstdint>

extern "C" {

__global__
void Background(uint32_t* samples, uint16_t* out) {
    // [prefix, sufix, best, best_position]
    __shared__ uint16_t counts[1024 * 4];

    int offset = blockDim.x * blockIdx.x;
    int cell = threadIdx.x * 4;

    bool same = samples[offset+threadIdx.x*2] == samples[offset+threadIdx.x*2+1];

    counts[cell] = same ? 2 : 1;
    counts[cell+1] = same ? 2 : 1;
    counts[cell+2] = same ? 2 : 1;
    counts[cell+3] = 0;

    for (int i = 2; i < blockDim.x / 2; i <<= 1) {
        __syncthreads();
        if (!(threadIdx.x & (i-1))) {
            int cell2 = cell+i*2;
            same = samples[offset+threadIdx.x*2+i] == samples[offset+threadIdx.x*2+i-1];

            if (counts[cell2+2] > counts[cell+2]) {
                counts[cell+2] = counts[cell2+2];
                counts[cell+3] = i + counts[cell2+3];
            }

            if (counts[cell+1] + counts[cell2] > counts[cell+2]) {
                counts[cell+2] = counts[cell+1] + counts[cell2];
                counts[cell+3] = i - counts[cell+1];
            }

            if (same && counts[cell] == i) counts[cell] = i + counts[cell2];
            counts[cell+1] = (same && counts[cell2+1] == i) ? counts[cell+1] + i : counts[cell2+1];
        }
    }

    if (threadIdx.x == 0) {
        out[blockIdx.x*4] = counts[0];
        out[blockIdx.x*4+1] = counts[1];
        out[blockIdx.x*4+2] = counts[2];
        out[blockIdx.x*4+3] = counts[3];
    }
}

}
