#include <cstdint>

#define ASSIGN_MIN(X, Y) X = X < Y ? X : Y
#define ASSIGN_MAX(X, Y) X = X > Y ? X : Y

#define TH_S 0.3
#define TH_V 0.2

extern "C" {

__global__
void Foreground(int n, double bg_s, double bg_v, uint32_t* samples) {
    int id = blockDim.x * blockIdx.x + threadIdx.x;
    if (id >= n) return;

    uint32_t x = samples[id];

    uint8_t m = x;
    uint8_t M = x;

    uint8_t y = x >> 8;
    ASSIGN_MIN(m, y);
    ASSIGN_MAX(M, y);

    y = x >> 16;
    ASSIGN_MIN(m, y);
    ASSIGN_MAX(M, y);

    double Md = (double)M / 256.0;
    double md = (double)m / 256.0;

    double diff_s = bg_s - (Md > 0.00 ? (Md - md) / Md : 0.0);
    double diff_v = bg_v - Md;

    if (diff_s < 0) diff_s *= -1;
    if (diff_v < 0) diff_v *= -1;

    // hacky, but we don't care about the last bit, the changed values
    // arent used anywhere anyway, and sorting by it will bring foreground
    // pixels to the beginnig of the array
    if (diff_s < TH_S && diff_v < TH_V) {
        samples[id] |= 0x01;
    } else {
        samples[id] &= 0xfffffffe;
    }
}

__global__
void AssignColors(int n, uint32_t * input, int * pointsClusters, uint32_t * centroids) {
    int id = blockDim.x * blockIdx.x + threadIdx.x;
    if (id >= n) return;

    int cluster = (input[id] & 1) ? 7 : pointsClusters[id];
    input[id] = centroids[cluster] | 0xff000000;
}

}
