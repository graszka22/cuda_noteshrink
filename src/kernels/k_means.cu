extern "C" {
    __global__
    void randomize(int* pointsClusters, int clusters, int elements) {
        int thid = blockIdx.x*blockDim.x+threadIdx.x;
        if(thid >= elements) return;
        pointsClusters[thid] = thid%clusters;
    }

    __global__
    void sumCentroids(uint* input, int* pointsClusters, int*  centroids_sums, 
    int*  centroids_count, int clusters, int elements) {
        int thid = blockIdx.x*blockDim.x+threadIdx.x;
        __shared__ uint sumsShared[3*1024];
        __shared__ uint countShared[1024];
        int cluster = 0;
        uint inputVal = 0;
        if(thid <= elements) {
            cluster = pointsClusters[thid];
            inputVal = input[thid];
        }
        uint x = (inputVal&0xFF0000)>>16;
        uint y = (inputVal&0xFF00)>>8;
        uint z = (inputVal&0xFF);
        for(int i = 0; i < clusters; ++i) {
            if(cluster == i && thid <= elements) {
                sumsShared[3*threadIdx.x] = x;
                sumsShared[3*threadIdx.x+1] = y;
                sumsShared[3*threadIdx.x+2] = z;
                countShared[threadIdx.x] = 1;
            }
            else {
                sumsShared[3*threadIdx.x] = 0;
                sumsShared[3*threadIdx.x+1] = 0;
                sumsShared[3*threadIdx.x+2] = 0;
                countShared[threadIdx.x] = 0;
            }
            __syncthreads();
            for(int off = 1; off < 1024; off *= 2) {
                uint tmpS1 = sumsShared[3*threadIdx.x];
                uint tmpS2 = sumsShared[3*threadIdx.x+1];
                uint tmpS3 = sumsShared[3*threadIdx.x+2];
                uint tmpC = countShared[threadIdx.x];
                __syncthreads();
                if(threadIdx.x+off < 1024) {
                    sumsShared[3*threadIdx.x+3*off] += tmpS1;
                    sumsShared[3*threadIdx.x+1+3*off] += tmpS2;
                    sumsShared[3*threadIdx.x+2+3*off] += tmpS3;
                    countShared[threadIdx.x+off] += tmpC;
                }
                __syncthreads();
            }
            if(threadIdx.x == 0) {
                centroids_sums[3*blockIdx.x*clusters+i*3] = sumsShared[1023*3];
                centroids_sums[3*blockIdx.x*clusters+i*3+1] = sumsShared[1023*3+1];
                centroids_sums[3*blockIdx.x*clusters+i*3+2] = sumsShared[1023*3+2];
                centroids_count[blockIdx.x*clusters+i] = countShared[1023];
            }
            __syncthreads();
        }
    }

    __global__
    void recomputeCentroids(uint* centroids, int* centroids_sums, 
    int* centroids_count, int clusters, int blocks) {
        int thid = blockIdx.x*blockDim.x+threadIdx.x;
        if(centroids_count[thid] == 0) return;
        unsigned long long sum1 = 0, sum2 = 0, sum3 = 0;
        int count = 0;

        for(int i = 0; i < blocks; ++i) {
            sum1 += centroids_sums[3*i*clusters+thid*3];
            sum2 += centroids_sums[3*i*clusters+thid*3+1];
            sum3 += centroids_sums[3*i*clusters+thid*3+2];
            count += centroids_count[i*clusters+thid];
        }
        uint centroidVal = 0;
        centroidVal |= (sum1/count)<<16;
        centroidVal |= (sum2/count)<<8;
        centroidVal |= (sum3/count);
        centroids[thid] = centroidVal;
    }

    __global__
    void pointsAssignment(uint* input, int* pointsClusters, uint* centroids, 
     int clusters, int elements) {
        int thid = blockIdx.x*blockDim.x+threadIdx.x;
        if(thid >= elements) return;
        int argmin = 0;
        int mindist = 256*256*3;
        uint inputVal = input[thid];
        int x = (inputVal&0xFF0000)>>16;
        int y = (inputVal&0xFF00)>>8;
        int z = (inputVal&0xFF);
        for(int i = 0; i < clusters; ++i) {
            uint centroidVal = centroids[i];
            int cx = (centroidVal&0xFF0000)>>16;
            int cy = (centroidVal&0xFF00)>>8;
            int cz = (centroidVal&0xFF);
            int dx = x-cx;
            int dy = y-cy;
            int dz = z-cz;
            int dist = dx*dx+dy*dy+dz*dz;
            if(dist < mindist) {
                argmin = i;
                mindist = dist;
            }
        }
        pointsClusters[thid] = argmin;
    }
}