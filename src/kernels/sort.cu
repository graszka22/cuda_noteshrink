extern "C" {

__global__
void Sort_prefix(int* inp, int* prefixbg, int* prefixg, int n, int mask) {
	__shared__ int prefix[2048];
	int idx = threadIdx.x * 2;
	int offset = 2 * blockDim.x * blockIdx.x + idx;

    prefix[idx] = (offset < n ? !(inp[offset] & mask) : 0);
    prefix[idx + 1] = (offset + 1 < n ? !(inp[offset + 1] & mask) : 0);

    int x, y;
    for (int i = 1; i < 2048; i <<= 1) {
        x = prefix[idx];
        y = prefix[idx + 1];

        __syncthreads();

        if (idx + i < 2048) { prefix[idx + i] += x; }
        if (idx + 1 + i < 2048) { prefix[idx + 1 + i] += y; }

        __syncthreads();
    }

    if (offset < n) prefixg[offset] = prefix[idx];
    if (offset + 1 < n) prefixg[offset + 1] = prefix[idx + 1];
    if (idx == 2046) prefixbg[blockIdx.x] = prefix[2047];
}

__global__
void Sort_pos(int* inp, int* out, int* prefixbg, int* prefixg, int n, int mask, int zeroes) {
	int idx = threadIdx.x * 2;
	int offset = 2 * blockDim.x * blockIdx.x + idx;
	int pref = blockIdx.x > 0 ? prefixbg[blockIdx.x - 1] : 0;

	int a1 = (offset < n ? inp[offset] : 0);
	int a2 = (offset + 1 < n ? inp[offset + 1] : 0);

	int p;

	if (offset < n) {
		p = (!!(a1 & mask)
			? zeroes + offset - (pref + prefixg[offset])
			: pref + prefixg[offset] - 1
		);
		out[p] = a1;
	}

	if (offset + 1 < n) {
		p = (!!(a2 & mask)
			? zeroes + offset + 1 - (pref + prefixg[offset + 1])
			: pref + prefixg[offset + 1] - 1
		);
		out[p] = a2;
	}
}

}
