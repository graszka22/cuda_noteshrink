#include <iostream>
#include <stdlib.h>
#include "noteshrink.h"
#include "cuda_driver.h"

int main(int argc, char *argv[]) {
    if (argc < 3) {
        fprintf(stderr, "usage: noteshrink <input> <output>\n");
        exit(1);
    }

    Noteshrink noteshrink(argv[1], argv[2]);
    noteshrink.run();
}
