#include <cstdlib>
#include <stdio.h>
#include <ctime>
#include <iostream>
#include <chrono>
#include "noteshrink.h"
#include "images.h"

void Noteshrink::kMeansClustering(CUDADeviceMemory inputData, CUDADeviceMemory centroids,
CUDADeviceMemory pointsClusters, int clusters, int elements) {
    int threadsPerBlock = 1024;
    int blocks = (elements+threadsPerBlock-1)/threadsPerBlock;

    CUDAModule k_meansModule = cuda_driver->loadModule("k_means.ptx");
    CUDAFunction randomize = k_meansModule.getFunction("randomize");
    CUDAFunction sumCentroids = k_meansModule.getFunction("sumCentroids");
    CUDAFunction recomputeCentroids = k_meansModule.getFunction("recomputeCentroids");
    CUDAFunction pointsAssignment = k_meansModule.getFunction("pointsAssignment");

    CUDADeviceMemory centroidsSums = cuda_driver->allocDeviceMemory(3*clusters*sizeof(int)*blocks);
    CUDADeviceMemory centroidsCount = cuda_driver->allocDeviceMemory(clusters*sizeof(int)*blocks);
    CUDAHostMemory<int> zeroedSums = cuda_driver->allocHostMemory<int>(3*clusters*blocks);
    CUDAHostMemory<int> zeroedCount = cuda_driver->allocHostMemory<int>(clusters*blocks);
    for(int i = 0; i < 3*clusters; ++i) zeroedSums[i] = 0;
    for(int i = 0; i < clusters; ++i) zeroedCount[i] = 0;

    randomize.launchKernel(blocks, threadsPerBlock, pointsClusters, &clusters, &elements);
    for(int i = 0; i < 30; ++i) {
        cuda_driver->copyHostToDevice(centroidsSums, zeroedSums);
        cuda_driver->copyHostToDevice(centroidsCount, zeroedCount);

        sumCentroids.launchKernel(blocks, threadsPerBlock, inputData, pointsClusters, centroidsSums,
            centroidsCount, &clusters, &elements);
        recomputeCentroids.launchKernel(1, clusters, centroids, centroidsSums, centroidsCount,
            &clusters, &blocks);
        pointsAssignment.launchKernel(blocks, threadsPerBlock, inputData, pointsClusters, centroids,
            &clusters, &elements);
    }
}

void Noteshrink::testKMeans() {
    int k = 8;
    int n = fg_sample_count;
    CUDAHostMemory<uint> inputData = cuda_driver->allocHostMemory<uint32_t>(n);
    CUDAHostMemory<uint> clusters = cuda_driver->allocHostMemory<uint32_t>(k);
    cuda_driver->copyHostToHost(inputData, sample);
    CUDADeviceMemory input = cuda_driver->allocDeviceMemory(n*sizeof(uint));
    CUDADeviceMemory centroids = cuda_driver->allocDeviceMemory(k*sizeof(uint));
    CUDADeviceMemory pointsClusters = cuda_driver->allocDeviceMemory(n*sizeof(int));
    cuda_driver->copyHostToDevice(input, inputData);

    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();

    kMeansClustering(input, centroids, pointsClusters, k, n);

    std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>( t2 - t1 ).count();
    std::cerr << "K-means time: " << duration << " ms" << std::endl;

    CUDAHostMemory<int> pointsClustersHost = cuda_driver->allocHostMemory<int>(n);
    cuda_driver->copyDeviceToHost(pointsClustersHost, pointsClusters);
    cuda_driver->copyDeviceToHost(clusters, centroids);
    std::cout << k << '\n';
    for(int i = 0; i < k; ++i)
        std::cout << ((clusters[i]&0xFF)) << ' '
            << ((clusters[i]&0xFF00)>>8) << ' '
            << ((clusters[i]&0xFF0000)>>16) << '\n';
    std::cout << n << '\n';
    for(int i = 0; i < n; ++i) {
        std::cout << ((inputData[i]&0xFF)) << ' '
            << ((inputData[i]&0xFF00)>>8) << ' '
            << ((inputData[i]&0xFF0000)>>16)
            << ' ' << pointsClustersHost[i] << '\n';
    }
}

void Noteshrink::pickSamples() {
    sample = cuda_driver->allocHostMemory<uint32_t>(SAMPLE_SIZE);
    srand(12345);
    uint32_t mask = 0xfff8f8f8;

    for (int i = 0; i < SAMPLE_SIZE; i++) {
        int x = rand() % (width * height);
        sample[i] = input_image[x] & mask;
    }
}

void Noteshrink::sort(int n, CUDADeviceMemory t, int bits, int * last_zeroes) {
    CUDAModule sort_module = cuda_driver->loadModule("sort.ptx");
    CUDAFunction sort_prefix = sort_module.getFunction("Sort_prefix");
    CUDAFunction sort_pos = sort_module.getFunction("Sort_pos");

	int blocks = (n + 2047) / 2048;

	CUDAHostMemory<int> prefixb = cuda_driver->allocHostMemory<int>(blocks);
	CUDADeviceMemory outdev0 = t;
	CUDADeviceMemory outdev1 = cuda_driver->allocDeviceMemory(n * sizeof(int));
	CUDADeviceMemory prefixdev = cuda_driver->allocDeviceMemory(n * sizeof(int));
	CUDADeviceMemory prefixbdev = cuda_driver->allocDeviceMemory(blocks * sizeof(int));

	for (int i = 0; i < bits; i++) {
		int mask = 1 << i;

        sort_prefix.launchKernel(blocks, 1024,
			(i % 2 == 0 ? outdev0 : outdev1),
			prefixbdev,
			prefixdev,
			&n,
			&mask
        );

        cuda_driver->copyDeviceToHost(prefixb, prefixbdev);

		for (int j = 1; j < blocks; j++) {
			prefixb[j] += prefixb[j - 1];
		}

		int zeroes = prefixb[blocks-1];

        cuda_driver->copyHostToDevice(prefixbdev, prefixb);

        sort_pos.launchKernel(blocks, 1024,
			(i % 2 == 0 ? outdev0 : outdev1),
			(i % 2 == 0 ? outdev1 : outdev0),
			prefixbdev,
			prefixdev,
			&n,
			&mask,
			&zeroes
        );

        if (last_zeroes) *last_zeroes = zeroes;
	}

    if (bits % 2) cuda_driver->copyDeviceToDevice(t, outdev1);
}

uint32_t Noteshrink::findBgColor() {
    CUDAModule module = cuda_driver->loadModule("background.ptx");
    CUDAFunction background = module.getFunction("Background");
    int blocks = SAMPLE_SIZE / 2048;

    // [prefix, sufix, best, best_position]
    CUDADeviceMemory outDev = cuda_driver->allocDeviceMemory(4*blocks*sizeof(uint16_t));
    CUDAHostMemory<uint16_t> outHost = cuda_driver->allocHostMemory<uint16_t>(4*blocks);

    cuda_driver->copyHostToDevice(sampleDev, sample);
    background.launchKernel(blocks, 1024, sampleDev, outDev);
    cuda_driver->copyDeviceToHost(outHost, outDev);

    int best_count = 0;
    uint32_t best = 0;
    int sufix_count = 0;
    uint32_t sufix = 0;

    for (int i = 0; i < blocks; i++) {
        if (sample[2048 * i] == sufix) {
            sufix_count += outHost[i*4];
            if (sufix_count > best_count) {
                best_count = sufix_count;
                best = sufix;
            }
        }

        uint32_t block_best = sample[2048 * i + outHost[i*4+3]];
        if (block_best != sufix && outHost[i*4+2] > best_count) {
            best_count = outHost[i*4+2];
            best = block_best;
        }

        if (sample[2048*i] != sample[2048*i+2047]) {
            sufix_count = outHost[i*4+1];
            sufix = sample[2048*i+2047];
        }
    }

    return best;
}

uint8_t _min(uint8_t a, uint8_t b) { return a < b ? a : b; }
uint8_t _max(uint8_t a, uint8_t b) { return a > b ? a : b; }

std::pair<double, double> rgb2sv(uint32_t x) {
    uint8_t m = x;
    m = _min(m, x >> 8);
    m = _min(m, x >> 16);

    uint8_t M = x;
    M = _max(M, x >> 8);
    M = _max(M, x >> 16);

    double Md = (double)M / 256.0;
    double md = (double)m / 256.0;

    double c = Md - md, v = Md;
    double s = v > 0.00 ? c / v : 0.0;
    return std::pair<double, double>(s, v);
}

double _abs(double a) {
    return a < 0 ? -a : a;
}

void Noteshrink::pickForegroundSamples(uint32_t bg) {
    auto y = rgb2sv(bg);

    CUDAModule module = cuda_driver->loadModule("foreground.ptx");
    CUDAFunction foreground = module.getFunction("Foreground");

    int ss = SAMPLE_SIZE;

    foreground.launchKernel(SAMPLE_SIZE / 1024, 1024, &ss, &y.first, &y.second, sampleDev);
    sort(SAMPLE_SIZE, sampleDev, 1, &fg_sample_count);
}

void Noteshrink::assignColors(uint32_t bg) {
    int fg_colors_count = 7;
    int pixel_count = width * height;

    CUDAHostMemory<uint32_t> centroidsHost = cuda_driver->allocHostMemory<uint32_t>(fg_colors_count+1);
    CUDADeviceMemory centroidsDev = cuda_driver->allocDeviceMemory((1+fg_colors_count)*sizeof(uint32_t));
    CUDADeviceMemory pointsClustersSampleDev = cuda_driver->allocDeviceMemory(fg_sample_count*sizeof(int));

    kMeansClustering(sampleDev, centroidsDev, pointsClustersSampleDev, fg_colors_count, fg_sample_count);
    cuda_driver->copyDeviceToHost(centroidsHost, centroidsDev);
    centroidsHost[7] = bg;
    cuda_driver->copyHostToDevice(centroidsDev, centroidsHost);

    CUDADeviceMemory inputDev = cuda_driver->allocDeviceMemory(pixel_count*sizeof(uint32_t));
    CUDADeviceMemory pointsClustersDev = cuda_driver->allocDeviceMemory(pixel_count*sizeof(int));
    // CUDAHostMemory<int> pointsClustersHost = cuda_driver->allocHostMemory(pixel_count);
    cuda_driver->copyHostToDevice(inputDev, input_image);

    CUDAModule k_meansModule = cuda_driver->loadModule("k_means.ptx");
    CUDAFunction pointsAssignment = k_meansModule.getFunction("pointsAssignment");

    int threadsPerBlock = 1024;
    int blocks = (pixel_count+threadsPerBlock-1)/threadsPerBlock;
    pointsAssignment.launchKernel(blocks, threadsPerBlock, inputDev, pointsClustersDev, centroidsDev, &fg_colors_count, &pixel_count);

    CUDAModule module_fg = cuda_driver->loadModule("foreground.ptx");
    CUDAFunction foreground = module_fg.getFunction("Foreground");
    CUDAFunction assign = module_fg.getFunction("AssignColors");

    auto y = rgb2sv(bg);
    foreground.launchKernel(blocks, threadsPerBlock, &pixel_count, &y.first, &y.second, inputDev);
    assign.launchKernel(blocks, threadsPerBlock, &pixel_count, inputDev, pointsClustersDev, centroidsDev);

    cuda_driver->copyDeviceToHost(input_image, inputDev);
}

void Noteshrink::run() {
    uint32_t * input_image_proxy;
    readJpegImage(input_image_name, &width, &height, &input_image_proxy);

    input_image = cuda_driver->allocHostMemory<uint32_t>(width*height);
    input_image.copyData(input_image_proxy, width*height*sizeof(uint32_t));

    pickSamples();

    sampleDev = cuda_driver->allocDeviceMemory(SAMPLE_SIZE * sizeof(uint32_t));
    cuda_driver->copyHostToDevice(sampleDev, sample);
    sort(SAMPLE_SIZE, sampleDev, 32, NULL);

    uint32_t bg = findBgColor();
    pickForegroundSamples(bg);
    assignColors(bg);

    // write modified image
    input_image.copyDataTo(input_image_proxy, width*height*sizeof(uint32_t));
    writePngImage(output_image_name, width, height, input_image_proxy);
    delete input_image_proxy;

    // write all samples
    // writePngImage(output_image_name, 100, 100, fg_sample);
    // write foreground samples
    // writePngImage(output_image_name, fg_sample_count / 20, 20, sample_proxy);
    // write modified image
    // writePngImage(output_image_name, width, height, input_image);
}
