#ifndef NOTESHRINK_H
#define NOTESHRINK_H

#include "cuda_driver.h"
#include <iostream>
#include <jpeglib.h>

// a multiple of 2048
#define SAMPLE_SIZE 10240

class Noteshrink {

public:
    Noteshrink(char * in, char * out) : input_image_name(in), output_image_name(out) {
        cuda_driver = new CUDADriver();
    }

    ~Noteshrink() {
        delete cuda_driver;
    }

    void run();

private:
    CUDADriver* cuda_driver;
    void kMeansClustering(CUDADeviceMemory inputData, CUDADeviceMemory centroids,
        CUDADeviceMemory pointsClusters, int clusters, int elements);
    void testKMeans();

	char * input_image_name;
	char * output_image_name;

	unsigned width;
	unsigned height;
	CUDAHostMemory<uint32_t> input_image;
	CUDAHostMemory<uint32_t> sample;
	CUDADeviceMemory sampleDev;
	int fg_sample_count;

	void pickSamples();
	void sort(int n, CUDADeviceMemory t, int bits, int * last_zeroes);
	uint32_t findBgColor();
	void pickForegroundSamples(uint32_t bg);
	void assignColors(uint32_t bg);
};

#endif
