import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
 
# Create data
K = int(input())
clusters = [list(map(int, input().split())) for _ in range(K)]
N = int(input())
data = [list(map(int, input().split())) for _ in range(N)]

# Create plot
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1, axisbg="1.0")
ax = fig.gca(projection='3d')

dataTransposed = np.array(data).transpose()
clustersTransposed = np.array(clusters).transpose()
col = list(map(lambda c: (clusters[c][0]/255.0, clusters[c][1]/255.0, clusters[c][2]/255.0), dataTransposed[3]))
col2 = list(map(lambda c: (c[0]/255.0, c[1]/255.0, c[2]/255.0), clustersTransposed))
ax.scatter(dataTransposed[0], dataTransposed[1], dataTransposed[2], alpha=0.8, c=col, edgecolors='none', s=30)
ax.scatter(clustersTransposed[0], clustersTransposed[1], clustersTransposed[2], alpha=0.8, c=col2, edgecolors='red', s=60)
plt.title('Matplot 3d scatter plot')
plt.show()